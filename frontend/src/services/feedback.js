import getBackendApi from "./backend-api";

const BASE_ROUTE = "feedback";

export default {
    async send(feedback) {
        return (await getBackendApi()).post(BASE_ROUTE, feedback);
    },
};