import firebase from "firebase/app";
import "firebase/analytics";

export default {
    logEvent(eventName, params) {
        firebase.analytics().logEvent(eventName, params);
    },
    setUserId(id) {
        firebase.analytics().setUserId(id);
    },
    setAnalyticsCollectionEnabled(enabled) {
        firebase.analytics().setAnalyticsCollectionEnabled(enabled);
    },
}