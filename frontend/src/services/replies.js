import getBackendApi from "./backend-api";

const BASE_ROUTE = "replies";

export default {
  async create(content, commentId) {
    const reply = {
      content: content,
      comment_id: commentId,
    };
    return (await getBackendApi()).post(BASE_ROUTE, reply);
  },
  async delete(replyId) {
    return (await getBackendApi()).delete(BASE_ROUTE + "/" + replyId);
  },
};
