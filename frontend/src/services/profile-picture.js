import ProfilePictureNotFound from "../assets/images/ProfilePictureNotFound.svg";

export function setProfilePictureNotFound(event) {
    event.target.src = ProfilePictureNotFound;
}