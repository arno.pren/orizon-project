import axios from "axios";
import firebaseAuthService from "./firebase-auth";
import store from "../store";

const nowTimestamp = +new Date();

export default async () => {
  const tokenExpirationTime = store.state.user.stsTokenManager
    ? store.state.user.stsTokenManager.expirationTime
    : store.state.user.b.c;
  // Refresh token 5 minutes before expiration
  const isTokenExpired = nowTimestamp > tokenExpirationTime - 1000 * 5 * 60;

  if (isTokenExpired) {
    await firebaseAuthService.refreshIdToken();
  }
  return axios.create({
    baseURL: process.env.VUE_APP_BACKEND_URL,
    headers: {
      Authorization: "Bearer " + store.state.idToken,
    },
  });
};
