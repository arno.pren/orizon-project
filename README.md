# Orizon

## Project description

Orizon is a goal-based social media application. It has been conceptualized and implemented by Guillaume Lafosse, Hugo
Lunel and Arno Prenveille between October 2020 and June 2021. We came up with this project in order to learn new
technologies and discover the world of startups.

The project is available at https://orizon-frontend-prod.herokuapp.com/ .

It is hosted on a free Heroku dyno. It can take up to 30 seconds before starting up if no one used it in the last 30
minutes.

## Architecture

## Context

As this project was created as a startup, we tried to create the simplest architecture possible, not taking into account
its scalability. The goal was to have a working MVP ASAP. In order to do so, we decided to:

- delegate the authentication to Firebase
- create the backend as a monolith
- use a cross-platform framework to build the frontend (Ionic VueJS) in order to be able to deploy it as a website at
  first and then deploy it for both Android and IOS, all with the same codebase

## Diagram

![architecture.png](architecture.png)

## Authors

- Guillaume Lafosse (Frontend)
- Hugo Lunel (Backend, CICD, a bit of frontend)
- Arno Prenveille (Backend, a bit of frontend)