import re
from datetime import timezone, datetime

from fastapi.exceptions import HTTPException
from starlette.status import HTTP_422_UNPROCESSABLE_ENTITY


def assert_timezone_offset_is_valid(date_time: datetime):
    if date_time and date_time.tzinfo != timezone.utc:
        tzinfo = str(date_time.tzinfo)
        time_zone_offset_hours, time_zone_offset_minutes = _get_timezone_offset(tzinfo)
        if (
            time_zone_offset_minutes != 0
            or time_zone_offset_hours < -12
            or time_zone_offset_hours > 14
        ):
            raise HTTPException(
                status_code=HTTP_422_UNPROCESSABLE_ENTITY,
                detail=f"Timezone offset {tzinfo} is not valid",
            )


def _get_timezone_offset(time_zone: str):
    time_zone_offset = re.split("[+-]", time_zone)[1].split(":")
    time_zone_offset_hours = int(time_zone_offset[0])
    time_zone_offset_minutes = int(time_zone_offset[1])
    if "-" in time_zone:
        time_zone_offset_hours *= -1
    return time_zone_offset_hours, time_zone_offset_minutes
