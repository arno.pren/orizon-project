from src.data_access import (
    assert_that_current_user_is_goal_owner,
    assert_that_current_user_is_post_owner,
    assert_that_post_is_accessible,
)
from src.data_validation.id import (
    assert_that_post_exists,
    assert_that_goal_exists,
)
from src.models import Post, User, Visibility
from src.schemas import RequestPost, ResponsePost, ResponseUserSlim
from tortoise.query_utils import Q


async def get_feed(
    offset: int,
    current_user: User,
) -> list[ResponsePost]:
    following_ids = [following.id async for following in current_user.following]
    posts = (
        await Post.filter(
            Q(goal__user_id__in=following_ids)
            | Q(goal__user_id=current_user.id)
        )
        .limit(10)
        .offset(offset)
        .order_by("-date")
    )
    return [
        await ResponsePost.from_tortoise_orm(
            post=post,
            current_user=current_user,
        )
        for post in posts
    ]


async def create(
    request_post: RequestPost,
    current_user: User,
) -> ResponsePost:
    await assert_that_goal_exists(request_post.goal_id)
    await assert_that_current_user_is_goal_owner(
        goal_id=request_post.goal_id,
        current_user_id=current_user.id,
    )
    return await ResponsePost.from_tortoise_orm(
        post=await Post.create(**request_post.dict(exclude_unset=True)),
        current_user=current_user,
    )


async def delete(
    post_id: int,
    current_user: User,
) -> None:
    await assert_that_post_exists(post_id)
    await assert_that_current_user_is_post_owner(
        post_id=post_id,
        current_user_id=current_user.id,
    )
    await Post.filter(id=post_id).delete()


async def like(
    post_id: int,
    current_user: User,
) -> None:
    post = await _get_post_if_exists_and_is_accessible(
        post_id=post_id,
        current_user=current_user,
    )
    await post.likers.add(current_user)


async def unlike(
    post_id: int,
    current_user: User,
) -> None:
    post = await _get_post_if_exists_and_is_accessible(
        post_id=post_id,
        current_user=current_user,
    )
    await post.likers.remove(current_user)


async def get_supporters(
    offset: int,
    post_id: int,
    current_user: User,
) -> list[ResponseUserSlim]:
    post = await _get_post_if_exists_and_is_accessible(
        post_id=post_id,
        current_user=current_user,
    )
    await post.fetch_related("likers")
    likers_ids = [liker.id for liker in post.likers]
    supporters = (
        await User.filter(Q(id__in=likers_ids))
        .limit(10)
        .offset(offset)
        .order_by("fullname")
    )
    return [ResponseUserSlim.from_tortoise_orm(user) for user in supporters]


async def _get_post_if_exists_and_is_accessible(
    post_id: int,
    current_user: User,
) -> Post:
    await assert_that_post_exists(post_id)
    await assert_that_post_is_accessible(
        post_id=post_id,
        current_user=current_user,
    )
    return await Post.get(id=post_id)
