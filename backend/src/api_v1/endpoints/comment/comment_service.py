from src.data_access import (
    assert_that_current_user_is_comment_owner,
    assert_that_post_is_accessible,
)
from src.data_validation.id import assert_that_comment_exists, assert_that_post_exists
from src.models import Comment, User
from src.schemas import RequestCreateComment, ResponseComment


async def get_all(
    post_id: int,
    offset: int,
    current_user: User,
) -> list[ResponseComment]:
    await assert_that_post_exists(post_id)
    await assert_that_post_is_accessible(
        post_id=post_id,
        current_user=current_user,
    )
    comments = (
        await Comment.filter(post_id=post_id).limit(10).offset(offset).order_by("-date")
    )
    return [await ResponseComment.from_tortoise_orm(comment) for comment in comments]


async def create(
    request_comment: RequestCreateComment,
    current_user: User,
) -> ResponseComment:
    await assert_that_post_exists(request_comment.post_id)
    await assert_that_post_is_accessible(
        request_comment.post_id,
        current_user=current_user,
    )
    return await ResponseComment.from_tortoise_orm(
        await Comment.create(
            **request_comment.dict(exclude_unset=True),
            writer_id=current_user.id,
        )
    )


async def delete(
    comment_id: int,
    current_user: User,
) -> None:
    await assert_that_comment_exists(comment_id)
    await assert_that_current_user_is_comment_owner(
        comment_id=comment_id,
        current_user_id=current_user.id,
    )
    await Comment.filter(id=comment_id).delete()
