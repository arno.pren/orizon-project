from src import firebase
from src.data_access import (
    assert_current_user_is_following_user,
    assert_current_user_is_followed_by_user,
    assert_current_user_is_not_following_user,
    assert_that_requester_is_in_user_follow_requests,
)
from src.data_validation.id import (
    assert_that_user_exists,
    assert_user_ids_are_different,
)
from src.models import User, Visibility
from src.schemas import ResponseUser, ResponseUserSlim
from tortoise.query_utils import Q


async def change_visibility(current_user: User, visibility: Visibility):
    await User.filter(id=current_user.id).update(visibility=visibility)
    async for requester in current_user.received_follow_requests:
        await accept_follow_request(requester.id, current_user)


async def get_by_id(
    user_id: int,
    current_user: User,
) -> ResponseUser:
    user = await _get_user_if_exists(user_id)
    return await ResponseUser.from_tortoise_orm(
        user=user,
        current_user=current_user,
    )


async def get_user_followers(
    user_id: int,
    offset: int,
    current_user: User,
) -> list[ResponseUser]:
    user = await _get_user_if_exists(user_id)
    if user_id != current_user.id and user.visibility == Visibility.PRIVATE:
        await assert_current_user_is_following_user(
            user=user,
            current_user=current_user,
        )
    follower_ids = [follower.id async for follower in user.followers]
    users = (
        await User.filter(id__in=follower_ids)
        .limit(10)
        .offset(offset)
        .order_by("fullname")
    )
    return [ResponseUserSlim.from_tortoise_orm(user) for user in users]


async def get_user_following(
    user_id: int,
    offset: int,
    current_user: User,
) -> list[ResponseUser]:
    user = await _get_user_if_exists(user_id)
    if user_id != current_user.id and user.visibility == Visibility.PRIVATE:
        await assert_current_user_is_following_user(
            user=user,
            current_user=current_user,
        )
    following_ids = [following.id async for following in user.following]
    users = (
        await User.filter(id__in=following_ids)
        .limit(10)
        .offset(offset)
        .order_by("fullname")
    )
    return [ResponseUserSlim.from_tortoise_orm(user) for user in users]


async def delete(current_user: User) -> None:
    firebase.delete_user(current_user.uid)
    await current_user.delete()


async def search(
    fullname: str,
    offset: int,
    current_user: User,
) -> list[ResponseUserSlim]:
    users = (
        await User.filter(
            Q(fullname__icontains=fullname),
            Q(id__not=current_user.id),
        )
        .limit(10)
        .offset(offset)
        .order_by("fullname")
    )
    return [ResponseUserSlim.from_tortoise_orm(user) for user in users]


async def follow(
    user_id: int,
    current_user: User,
) -> str:
    user = await _get_user_if_exists(user_id)
    assert_user_ids_are_different(user.id, current_user.id)
    await assert_current_user_is_not_following_user(
        user=user,
        current_user=current_user,
    )
    if user.visibility == Visibility.PUBLIC:
        await user.followers.add(current_user)
        return f"Successfully followed user with id {user_id}"
    else:
        await user.received_follow_requests.add(current_user)
        return f"Successfully requested to follow user with id {user_id}"


async def accept_follow_request(
    requester_id: int,
    current_user: User,
) -> None:
    requester = await _get_user_if_exists(requester_id)
    await assert_that_requester_is_in_user_follow_requests(
        requester=requester,
        user=current_user,
    )
    await current_user.received_follow_requests.remove(requester)
    await current_user.followers.add(requester)


async def deny_follow_request(
    requester_id: int,
    current_user: User,
) -> None:
    requester = await _get_user_if_exists(requester_id)
    await assert_that_requester_is_in_user_follow_requests(
        requester=requester,
        user=current_user,
    )
    await current_user.received_follow_requests.remove(requester)


async def cancel_follow_request(
    user_id: int,
    current_user: User,
) -> None:
    user = await _get_user_if_exists(user_id)
    await assert_that_requester_is_in_user_follow_requests(
        requester=current_user,
        user=user,
    )
    await user.received_follow_requests.remove(current_user)


async def unfollow(
    user_id: int,
    current_user: User,
) -> None:
    user = await _get_user_if_exists(user_id)
    assert_user_ids_are_different(user.id, current_user.id)
    await assert_current_user_is_following_user(
        user=user,
        current_user=current_user,
    )
    await user.followers.remove(current_user)


async def remove_follower(
    user_id: int,
    current_user: User,
) -> None:
    follower = await _get_user_if_exists(user_id)
    assert_user_ids_are_different(follower.id, current_user.id)
    await assert_current_user_is_followed_by_user(
        user=follower,
        current_user=current_user,
    )
    await current_user.followers.remove(follower)


async def _get_user_if_exists(user_id: int) -> User:
    await assert_that_user_exists(user_id)
    return await User.get(id=user_id)
