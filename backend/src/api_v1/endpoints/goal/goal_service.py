from src.data_access import (
    assert_that_current_user_is_goal_owner,
    assert_that_goal_is_accessible,
)
from src.data_validation.datetime import assert_timezone_offset_is_valid

from src.data_validation.id import assert_that_goal_exists
from src.models import Goal, Step, User
from src.schemas import (
    RequestGoal,
    RequestStepWithoutGoalId,
    ResponseGoal,
    ResponseGoalSlim,
)


async def get_all(
    offset: int,
    current_user: User,
) -> list[ResponseGoalSlim]:
    goals = (
        await Goal.filter(user_id=current_user.id)
        .limit(10)
        .offset(offset)
        .order_by("-creation_date")
    )
    return [ResponseGoalSlim.from_tortoise_orm(goal) for goal in goals]


async def get_by_id(
    goal_id: int,
    current_user: User,
) -> ResponseGoal:
    await assert_that_goal_exists(goal_id)
    await assert_that_goal_is_accessible(
        goal_id=goal_id,
        current_user=current_user,
    )
    return await ResponseGoal.from_tortoise_orm(
        goal=await Goal.get(id=goal_id),
        current_user=current_user,
    )


async def create(
    request_goal: RequestGoal,
    request_steps: list[RequestStepWithoutGoalId],
    current_user: User,
) -> ResponseGoalSlim:
    goal = await Goal.create(
        **request_goal.dict(exclude_unset=True),
        user_id=current_user.id,
    )
    await _create_steps(
        request_steps=request_steps,
        goal_id=goal.id,
    )
    return ResponseGoalSlim.from_tortoise_orm(goal)


async def update(
    goal_id: int,
    request_goal: RequestGoal,
    request_steps: list[RequestStepWithoutGoalId],
    current_user: User,
) -> ResponseGoal:
    await assert_that_goal_exists(goal_id)
    await assert_that_current_user_is_goal_owner(
        goal_id=goal_id,
        current_user_id=current_user.id,
    )
    await Goal.filter(id=goal_id).update(**request_goal.dict(exclude_unset=True))
    await Step.filter(goal_id=goal_id).delete()
    await _create_steps(
        request_steps=request_steps,
        goal_id=goal_id,
    )
    return await ResponseGoal.from_tortoise_orm(
        goal=await Goal.get(id=goal_id),
        current_user=current_user,
    )


async def delete(
    goal_id: int,
    current_user: User,
) -> None:
    await assert_that_goal_exists(goal_id)
    await assert_that_current_user_is_goal_owner(
        goal_id=goal_id,
        current_user_id=current_user.id,
    )
    await Goal.filter(id=goal_id).delete()


async def search(
    goal_title: str,
    current_user: User,
) -> list[ResponseGoalSlim]:
    goals = (
        await Goal.filter(
            title__icontains=goal_title,
            user__id=current_user.id,
        )
        .limit(3)
        .order_by("title")
    )
    return [ResponseGoalSlim.from_tortoise_orm(goal) for goal in goals]


async def _create_steps(
    request_steps: list[RequestStepWithoutGoalId],
    goal_id: int,
):
    for request_step in request_steps:
        assert_timezone_offset_is_valid(request_step.completion_date)
        await Step.create(
            **request_step.dict(exclude_unset=True),
            goal_id=goal_id,
        )
