from fastapi import APIRouter
from src.api_v1.endpoints.comment import comment_controller
from src.api_v1.endpoints.goal import goal_controller
from src.api_v1.endpoints.post import post_controller
from src.api_v1.endpoints.reply import reply_controller
from src.api_v1.endpoints.user import user_controller
from src.api_v1.endpoints.feedback import feedback_controller

api_router = APIRouter()
api_router.include_router(
    post_controller.router,
    tags=["posts"],
    prefix="/posts",
)
api_router.include_router(
    user_controller.router,
    tags=["users"],
    prefix="/users",
)
api_router.include_router(
    comment_controller.router,
    tags=["comments"],
    prefix="/comments",
)
api_router.include_router(
    goal_controller.router,
    tags=["goals"],
    prefix="/goals",
)
api_router.include_router(
    reply_controller.router,
    tags=["replies"],
    prefix="/replies",
)

api_router.include_router(
    feedback_controller.router,
    tags=["feedback"],
    prefix="/feedback",
)
