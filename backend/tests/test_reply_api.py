import pytest
from httpx import AsyncClient
from src.models import Reply
from tests.conftest import assert_model_objects_are_equal

pytestmark = pytest.mark.asyncio

BASE_ROUTE = "/replies"


async def test_create_reply_ownPost(
    async_client: AsyncClient,
    freeze_time,
):
    # given
    comment_id_on_current_user_post = 1
    utc_now = freeze_time
    request_body = {
        "content": "this is my reply",
        "comment_id": comment_id_on_current_user_post,
    }
    expected_object = Reply(
        content="this is my reply",
        date=utc_now,
        comment_id=comment_id_on_current_user_post,
        writer_id=1,
    )

    # when
    response = await async_client.post(
        BASE_ROUTE,
        json=request_body,
    )

    # then
    assert response.status_code == 200
    reply = response.json()
    expected_reply_schema = {
        "id": 3,
        "content": "this is my reply",
        "date": utc_now,
        "writer": {
            "id": 1,
            "uid": "CurrentUserUid",
            "fullname": "current_user_firstname current_user_lastname",
            "profile_picture_url": "current_user_profile_picture_url",
        },
    }
    assert reply == expected_reply_schema
    assert_model_objects_are_equal(await Reply.get(id=reply["id"]), expected_object)


async def test_create_reply_followingPost(
    async_client: AsyncClient,
    freeze_time,
    current_user_follow_other_user,
):
    # given
    comment_id_on_other_user_post = 2
    utc_now = freeze_time
    request_body = {
        "content": "this is my reply",
        "comment_id": comment_id_on_other_user_post,
    }
    expected_object = Reply(
        content="this is my reply",
        date=utc_now,
        comment_id=comment_id_on_other_user_post,
        writer_id=1,
    )

    # when
    response = await async_client.post(
        BASE_ROUTE,
        json=request_body,
    )

    # then
    assert response.status_code == 200
    reply = response.json()
    expected_reply_schema = {
        "id": 3,
        "content": "this is my reply",
        "date": utc_now,
        "writer": {
            "id": 1,
            "uid": "CurrentUserUid",
            "fullname": "current_user_firstname current_user_lastname",
            "profile_picture_url": "current_user_profile_picture_url",
        },
    }
    assert reply == expected_reply_schema
    assert_model_objects_are_equal(await Reply.get(id=reply["id"]), expected_object)


async def test_create_reply_withPostIsPublic(
    async_client: AsyncClient,
    freeze_time,
    make_other_user_public,
):
    # given
    comment_id_on_other_user_post = 2
    utc_now = freeze_time
    request_body = {
        "content": "this is my reply",
        "comment_id": comment_id_on_other_user_post,
    }
    expected_object = Reply(
        content="this is my reply",
        date=utc_now,
        comment_id=comment_id_on_other_user_post,
        writer_id=1,
    )

    # when
    response = await async_client.post(
        BASE_ROUTE,
        json=request_body,
    )

    # then
    assert response.status_code == 200
    reply = response.json()
    expected_reply_schema = {
        "id": 3,
        "content": "this is my reply",
        "date": utc_now,
        "writer": {
            "id": 1,
            "uid": "CurrentUserUid",
            "fullname": "current_user_firstname current_user_lastname",
            "profile_picture_url": "current_user_profile_picture_url",
        },
    }
    assert reply == expected_reply_schema
    assert_model_objects_are_equal(await Reply.get(id=reply["id"]), expected_object)


async def test_create_reply_withCommentIdDoesNotExist(async_client: AsyncClient):
    # given
    request_body = {
        "content": "this is my reply",
        "comment_id": 99,
    }
    expected_response = {"detail": "Comment with id 99 does not exist"}

    # when
    response = await async_client.post(
        BASE_ROUTE,
        json=request_body,
    )

    # then
    assert response.status_code == 404
    assert response.json() == expected_response


async def test_create_reply_withEmptyContent(async_client: AsyncClient):
    # given
    request_body = {
        "content": "",
        "comment_id": 1,
    }
    expected_response = {"detail": "content field can not be empty"}

    # when
    response = await async_client.post(
        BASE_ROUTE,
        json=request_body,
    )

    # then
    assert response.status_code == 422
    assert response.json() == expected_response


async def test_delete_reply(async_client: AsyncClient):
    # given
    reply_id = 1
    expected_response = "Successfully deleted reply with id 1"

    # when
    response = await async_client.delete(f"{BASE_ROUTE}/{reply_id}")

    # then
    assert response.status_code == 200
    assert response.json() == expected_response
    assert not await Reply.exists(id=reply_id)


async def test_delete_reply_withAccessForbidden(async_client: AsyncClient):
    # given
    other_user_reply_id = 2
    expected_response = {"detail": "Access forbidden"}

    # when
    response = await async_client.delete(f"{BASE_ROUTE}/{other_user_reply_id}")

    # then
    assert response.status_code == 403
    assert response.json() == expected_response
    assert await Reply.exists(id=other_user_reply_id)


async def test_delete_reply_withIdDoesNotExist(async_client: AsyncClient):
    # given
    unexisting_reply_id = 99
    expected_response = {"detail": "Reply with id 99 does not exist"}

    # when
    response = await async_client.delete(f"{BASE_ROUTE}/{unexisting_reply_id}")

    # then
    assert response.status_code == 404
    assert response.json() == expected_response
