import pytest
from httpx import AsyncClient
from src.models import Post
from tests.conftest import assert_model_objects_are_equal

BASE_ROUTE = "/posts"

pytestmark = pytest.mark.asyncio


async def test_get_feed_posts(
    async_client: AsyncClient,
    freeze_time,
):
    # when
    response = await async_client.get(BASE_ROUTE)

    # then
    assert response.status_code == 200
    posts = response.json()
    assert len(posts) == 1
    utc_now = freeze_time
    expected_current_user_post = {
        "id": 1,
        "date": utc_now,
        "description": "current user post description",
        "number_of_comments_and_replies": 2,
        "number_of_likes": 0,
        "goal": {
            "id": 1,
            "status": "TODO",
            "title": "current user goal title",
            "user": {
                "id": 1,
                "uid": "CurrentUserUid",
                "fullname": "current_user_firstname current_user_lastname",
                "profile_picture_url": "current_user_profile_picture_url",
            },
        },
        "is_liked": False,
    }
    assert posts[0] == expected_current_user_post


async def test_get_feed_posts_withCurrentUserFollowOtherUser(
    async_client: AsyncClient,
    freeze_time,
    current_user_follow_other_user,
):
    # when
    response = await async_client.get(BASE_ROUTE)

    # then
    assert response.status_code == 200
    posts = response.json()
    assert len(posts) == 2
    utc_now = freeze_time
    expected_current_user_post = {
        "id": 1,
        "date": utc_now,
        "description": "current user post description",
        "number_of_comments_and_replies": 2,
        "number_of_likes": 0,
        "goal": {
            "id": 1,
            "status": "TODO",
            "title": "current user goal title",
            "user": {
                "id": 1,
                "uid": "CurrentUserUid",
                "fullname": "current_user_firstname current_user_lastname",
                "profile_picture_url": "current_user_profile_picture_url",
            },
        },
        "is_liked": False,
    }
    assert posts[0] == expected_current_user_post
    expected_other_user_post = {
        "id": 2,
        "date": utc_now,
        "description": "other user post description",
        "number_of_comments_and_replies": 2,
        "number_of_likes": 0,
        "goal": {
            "id": 2,
            "status": "TODO",
            "title": "other user goal title",
            "user": {
                "id": 2,
                "uid": "OtherUserUid",
                "fullname": "other_user_firstname other_user_lastname",
                "profile_picture_url": "other_user_profile_picture_url",
            },
        },
        "is_liked": False,
    }
    assert posts[1] == expected_other_user_post


async def test_get_feed_posts_withCurrentUserDoesNotFollowOtherUser(
    async_client: AsyncClient,
    freeze_time,
    make_other_user_public,
):
    # when
    response = await async_client.get(BASE_ROUTE)

    # then
    assert response.status_code == 200
    posts = response.json()
    assert len(posts) == 1
    utc_now = freeze_time
    expected_current_user_post = {
        "id": 1,
        "date": utc_now,
        "description": "current user post description",
        "number_of_comments_and_replies": 2,
        "number_of_likes": 0,
        "goal": {
            "id": 1,
            "status": "TODO",
            "title": "current user goal title",
            "user": {
                "id": 1,
                "uid": "CurrentUserUid",
                "fullname": "current_user_firstname current_user_lastname",
                "profile_picture_url": "current_user_profile_picture_url",
            },
        },
        "is_liked": False,
    }
    assert posts[0] == expected_current_user_post


async def test_get_supporters(
    async_client: AsyncClient,
    current_user_likes_other_user_post,
    make_other_user_public,
):
    # given
    post_id = 2

    # when
    response = await async_client.get(f"{BASE_ROUTE}/{post_id}/supporters")

    # then
    assert response.status_code == 200
    users = response.json()
    assert len(users) == 1
    expected_user_schema = {
        "id": 1,
        "uid": "CurrentUserUid",
        "fullname": "current_user_firstname current_user_lastname",
        "profile_picture_url": "current_user_profile_picture_url",
    }
    assert users[0] == expected_user_schema


async def test_get_supporters_withPostDoesNotExist(async_client: AsyncClient):
    # given
    post_id = 99

    # when
    response = await async_client.get(f"{BASE_ROUTE}/{post_id}/supporters")

    # then
    assert response.status_code == 404
    expected_response = {"detail": "Goal with id 99 does not exist"}


async def test_get_supporters_withAccessForbidden(async_client: AsyncClient):
    # given
    post_id = 2

    # when
    response = await async_client.get(f"{BASE_ROUTE}/{post_id}/supporters")

    # then
    assert response.status_code == 403
    expected_response = {"detail": "Access forbidden"}


async def test_create_post(
    async_client: AsyncClient,
    freeze_time,
):
    # given
    current_user_post_id = 1
    request_body = {
        "description": "my description",
        "goal_id": current_user_post_id,
    }

    # when
    response = await async_client.post(
        BASE_ROUTE,
        json=request_body,
    )

    # then
    assert response.status_code == 200
    post = response.json()
    utc_now = freeze_time
    expected_current_user_post = {
        "id": 3,
        "date": utc_now,
        "description": "my description",
        "number_of_comments_and_replies": 0,
        "number_of_likes": 0,
        "goal": {
            "id": 1,
            "status": "TODO",
            "title": "current user goal title",
            "user": {
                "id": 1,
                "uid": "CurrentUserUid",
                "fullname": "current_user_firstname current_user_lastname",
                "profile_picture_url": "current_user_profile_picture_url",
            },
        },
        "is_liked": False,
    }
    assert post == expected_current_user_post
    expected_post_object = Post(
        description="my description",
        date=utc_now,
        goal_id=current_user_post_id,
    )
    assert_model_objects_are_equal(await Post.get(id=post["id"]), expected_post_object)


async def test_create_post_withAccessForbidden(async_client: AsyncClient):
    # given
    other_user_goal_id = 2
    request_body = {
        "description": "my description",
        "goal_id": other_user_goal_id,
    }
    expected_response = {"detail": "Access forbidden"}

    # when
    response = await async_client.post(
        BASE_ROUTE,
        json=request_body,
    )
    # then
    assert response.status_code == 403
    assert response.json() == expected_response


async def test_create_post_withGoalIdDoesNotExist(async_client: AsyncClient):
    # given
    request_body = {
        "description": "my description",
        "goal_id": 99,
    }
    expected_response = {"detail": "Goal with id 99 does not exist"}

    # when
    response = await async_client.post(
        BASE_ROUTE,
        json=request_body,
    )

    # then
    assert response.status_code == 404
    assert response.json() == expected_response


async def test_delete_post(async_client: AsyncClient):
    # given
    post_id = 1
    expected_response = "Successfully deleted post with id 1"

    # when
    response = await async_client.delete(f"{BASE_ROUTE}/{post_id}")

    # then
    assert response.status_code == 200
    assert response.json() == expected_response
    assert not await Post.exists(id=post_id)


async def test_delete_post_withAccessForbidden(async_client: AsyncClient):
    # given
    other_user_post_id = 2
    expected_response = {"detail": "Access forbidden"}

    # when
    response = await async_client.delete(f"{BASE_ROUTE}/{other_user_post_id}")

    # then
    assert response.status_code == 403
    assert response.json() == expected_response
    assert await Post.exists(id=other_user_post_id)


async def test_delete_post_withIdDoesNotExist(async_client: AsyncClient):
    # given
    unexisting_post_id = 99
    expected_response = {"detail": "Post with id 99 does not exist"}

    # when
    response = await async_client.delete(f"{BASE_ROUTE}/{unexisting_post_id}")

    # then
    assert response.status_code == 404
    assert response.json() == expected_response


async def test_like_post_ownPost(async_client: AsyncClient):
    # given
    current_user_post_id = 1
    expected_response = "Successfully liked post with id 1"

    # when
    response = await async_client.post(f"{BASE_ROUTE}/{current_user_post_id}/like")

    # then
    assert response.status_code == 200
    assert response.json() == expected_response
    post = await Post.get(id=current_user_post_id).prefetch_related("likers")
    likers = post.likers
    assert len(likers) == 1
    assert likers[0].uid == "CurrentUserUid"


async def test_like_post_followingPost(
    async_client: AsyncClient,
    current_user_follow_other_user,
):
    # given
    other_user_post_id = 2
    expected_response = "Successfully liked post with id 2"

    # when
    response = await async_client.post(f"{BASE_ROUTE}/{other_user_post_id}/like")

    # then
    assert response.status_code == 200
    assert response.json() == expected_response
    post = await Post.get(id=other_user_post_id).prefetch_related("likers")
    likers = post.likers
    assert len(likers) == 1
    assert likers[0].uid == "CurrentUserUid"


async def test_like_post_withPostIsPublic(
    async_client: AsyncClient,
    make_other_user_public,
):
    # given
    other_user_post_id = 2
    expected_response = "Successfully liked post with id 2"

    # when
    response = await async_client.post(f"{BASE_ROUTE}/{other_user_post_id}/like")

    # then
    assert response.status_code == 200
    assert response.json() == expected_response
    post = await Post.get(id=other_user_post_id).prefetch_related("likers")
    likers = post.likers
    assert len(likers) == 1
    assert likers[0].uid == "CurrentUserUid"


async def test_like_post_withAccessForbidden(async_client: AsyncClient):
    # given
    other_user_post_id = 2
    expected_response = {"detail": "Access forbidden"}

    # when
    response = await async_client.post(f"{BASE_ROUTE}/{other_user_post_id}/like")

    # then
    assert response.status_code == 403
    assert response.json() == expected_response


async def test_like_post_withPostIdDoesNotExist(async_client: AsyncClient):
    # given
    unexisting_post_id = 99
    expected_response = {"detail": "Post with id 99 does not exist"}

    # when
    response = await async_client.post(f"{BASE_ROUTE}/{unexisting_post_id}/like")

    # then
    assert response.status_code == 404
    assert response.json() == expected_response


async def test_unlike_post_ownPost(
    async_client: AsyncClient,
    current_user_likes_its_post,
):
    # given
    current_user_post_id = 1
    expected_response = "Successfully unliked post with id 1"

    # when
    response = await async_client.post(f"{BASE_ROUTE}/{current_user_post_id}/unlike")

    # then
    assert response.status_code == 200
    assert response.json() == expected_response
    post = await Post.get(id=current_user_post_id).prefetch_related("likers")
    assert len(post.likers) == 0


async def test_unlike_post_followingPost(
    async_client: AsyncClient,
    current_user_follow_other_user,
    current_user_likes_other_user_post,
):
    # given
    other_user_post_id = 2
    expected_response = "Successfully unliked post with id 2"

    # when
    response = await async_client.post(f"{BASE_ROUTE}/{other_user_post_id}/unlike")

    # then
    assert response.status_code == 200
    assert response.json() == expected_response
    post = await Post.get(id=other_user_post_id).prefetch_related("likers")
    assert len(post.likers) == 0


async def test_unlike_post_withPostIsPublic(
    async_client: AsyncClient,
    make_other_user_public,
    current_user_likes_other_user_post,
):
    # given
    other_user_post_id = 2
    expected_response = "Successfully unliked post with id 2"

    # when
    response = await async_client.post(f"{BASE_ROUTE}/{other_user_post_id}/unlike")

    # then
    assert response.status_code == 200
    assert response.json() == expected_response
    post = await Post.get(id=other_user_post_id).prefetch_related("likers")
    assert len(post.likers) == 0


async def test_unlike_post_withAccessForbidden(async_client: AsyncClient):
    # given
    other_user_post_id = 2
    expected_response = {"detail": "Access forbidden"}

    # when
    response = await async_client.post(f"{BASE_ROUTE}/{other_user_post_id}/unlike")

    # then
    assert response.status_code == 403
    assert response.json() == expected_response


async def test_unlike_post_withPostIdDoesNotExist(async_client: AsyncClient):
    # given
    unexisting_post_id = 99
    expected_response = {"detail": "Post with id 99 does not exist"}

    # when
    response = await async_client.post(f"{BASE_ROUTE}/{unexisting_post_id}/unlike")

    # then
    assert response.status_code == 404
    assert response.json() == expected_response
