import pytest
from src.models import (
    Comment,
    Goal,
    GoalStatus,
    Post,
    Reply,
    Step,
    User,
    Visibility,
)

pytestmark = pytest.mark.asyncio


@pytest.fixture
async def prepare_current_user_data(freeze_time):
    utc_now = freeze_time
    user = await User.create(
        uid="CurrentUserUid",
        email="current.user@gmail.com",
        fullname="current_user_firstname current_user_lastname",
        profile_picture_url="current_user_profile_picture_url",
        visibility=Visibility.PUBLIC,
    )
    goal = await Goal.create(
        title="current user goal title",
        description="current user goal description",
        status=GoalStatus.TODO,
        user=user,
    )
    await Step.create(
        description="current user step description",
        completion_date=utc_now,
        goal=goal,
    )
    post = await Post.create(
        description="current user post description",
        goal=goal,
    )
    comment = await Comment.create(
        content="current user comment content",
        post=post,
        writer=user,
    )
    await Reply.create(
        content="current user reply content",
        comment=comment,
        writer=user,
    )


@pytest.fixture
async def prepare_other_user_data(freeze_time):
    utc_now = freeze_time
    user = await User.create(
        uid="OtherUserUid",
        email="other.user@gmail.com",
        fullname="other_user_firstname other_user_lastname",
        profile_picture_url="other_user_profile_picture_url",
        visibility=Visibility.PRIVATE,
    )
    goal = await Goal.create(
        title="other user goal title",
        description="other user goal description",
        status=GoalStatus.TODO,
        user=user,
    )
    await Step.create(
        description="other user step description",
        completion_date=utc_now,
        goal=goal,
    )
    post = await Post.create(
        description="other user post description",
        goal=goal,
    )
    comment = await Comment.create(
        content="other user comment content",
        post=post,
        writer=user,
    )
    await Reply.create(
        content="other user reply content",
        comment=comment,
        writer=user,
    )


@pytest.fixture
async def current_user_likes_its_post():
    current_user = await _get_current_user()
    current_user_post = await Post.get(goal__user_id=1)
    await current_user_post.likers.add(current_user)


@pytest.fixture
async def current_user_likes_other_user_post():
    current_user = await _get_current_user()
    other_user_post = await Post.get(goal__user_id=2)
    await other_user_post.likers.add(current_user)


@pytest.fixture
async def current_user_follow_other_user():
    current_user = await _get_current_user()
    other_user = await _get_other_user()
    await other_user.followers.add(current_user)


@pytest.fixture
async def other_user_follow_current_user():
    current_user = await _get_current_user()
    other_user = await _get_other_user()
    await current_user.followers.add(other_user)


@pytest.fixture
async def other_user_request_follow_current_user():
    current_user = await _get_current_user()
    other_user = await _get_other_user()
    await current_user.received_follow_requests.add(other_user)


@pytest.fixture
async def current_user_request_follow_other_user():
    current_user = await _get_current_user()
    other_user = await _get_other_user()
    await other_user.received_follow_requests.add(current_user)


@pytest.fixture
async def make_other_user_public():
    await User.filter(uid="OtherUserUid").update(visibility=Visibility.PUBLIC)


@pytest.fixture
async def make_current_user_private():
    await User.filter(uid="CurrentUserUid").update(visibility=Visibility.PRIVATE)


async def _get_current_user() -> User:
    return await User.get(uid="CurrentUserUid")


async def _get_other_user() -> User:
    return await User.get(uid="OtherUserUid")
